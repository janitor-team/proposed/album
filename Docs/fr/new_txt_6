Cr�ation de th�mes

<span lang="fr">
ITEM: M�todes

Il y a des moyens faciles et d'autres compliqu�s pour cr�er un th�me sur
mesure en fonction de ce que vous voulez faire.

ITEM: Editer le code HTML d'un th�me existant

Si vous voulez juste modifier l�g�rement un th�me afin qu'il corresponde �
l'environnement de votre site, il semble opportun d'�diter un th�me existant.
Dans le r�pertoire du th�me, il y a deux fichiers avec l'extension
th. album.th est le patron pour les pages de l'album (les pages des vignettes)
et image.th est le patron pour les pages des images (o� vous voyez les images
moyennement r�duites ou en pleine taille). Ces fichiers sont tr�s similaire �
du code HTML except� quelques rajouts de code <: ePerl :>. M�me si vous ne
connaissez ni Perl ni ePerl, vous pouvez n�anmoins �diter le code HTML pour y
apporter des changements basiques.

Des th�mes pour bien d�marrer :

N'importe quel th�me dans simmer_theme peut servir comme "Blue" ou "Maste". Si
vous avez seulement besoin de 4 bordures aux coins de vos vignettes, alors
jetez un oeil sur "Eddie Bauer". Si vous voulez des bordures transparentes ou
qui se recouvrent, regardez "FunLand".

Si vous voulez quelque chose avec un minimum de code, essayez "simple" mais
attention, je n'ai pas mis � jour ce th�me depuis un bon bout de temps.

ITEM: Le moyen facile : simmer_theme pour les graphiques

La plupart des th�mes ont un m�me format basique, montrent une s�rie de
vignettes pour des r�pertoires puis pour des images avec une bordure et une
ligne graphiques ainsi qu'un fond d'�cran. Si c'est ce que vous voulez mais
que vous souhaitez cr�er de nouveaux graphiques, il y a un outil qui vous
permettra de construire des th�mes.

Si vous cr�ez un r�pertoire avec des images et un fichier CREDIT ainsi qu'un
fichier Font ou Style.css, alors l'utilitaire simmer_theme vous permettra de
r�aliser des th�mes.

Fichiers :
<b>Font/Style.css</b>
Ce fichier d�termine les fontes utilis�es par le th�me. Le fichier "Font" est
le plus ancien syst�me, d�crit ci-dessous. Cr�ez un fichier Style.css et
d�finissez des styles pour : body (le corps), title (le titre), main (la page
principale), credit (le cr�dit) et tout ce que vous voulez.

Regardez FunLand pour un exemple basique.

Autrement, utilisez un fichier Font. Un exemple de fichier Font est :

--------------------------------------------------
$TITLE_FONT = "size='4' color='#ffffff' face='Times New Roman,Georgia,Times'";
$MAIN_FONT = "face='Times New Roman,Georgia,Times'";
$CREDIT_FONT = "size='-1' face='Verdana' color='#ffffff'";
$BODY = "background='$PATH/bkgrnd.gif' link='#0099FF'">
--------------------------------------------------


<b>CREDIT</b>
Le fichier de cr�dit comporte deux lignes et est utilis� pour l'index des
th�mes � MarginalHacks. Si vous ne nous soumettez jamais votre th�me, alors
vous n'avez pas besoin de ce fichier (mais faites-le s'il vous pla�t !). En
tout cas, les deux lignes (et seulement deux lignes) sont :

1) une courte description du th�me (� faire tenir sur une seule ligne),
2) Votre nom (qui peut �tre � l'int�rieur d'un mailto: ou d'un lien internet).

<b>Null.gif</b>
Chaque th�me cr�� par simmer a besoin d'une image "espace" c'est-�-dire une
image au format gif transparent de dimensions 1x1. Vous pouvez simplement la
copier depuis un autre th�me cr�� par simmer.

Fichiers d'images optionnels :

<b>Images de la barre</b>
La barre est compos�e de Bar_L.gif (� gauche), Bar_R.gif (� droite) et
Bar_M.gif au milieu. L'image de la barre du milieu est �tir�e. Si vous avez
besoin d'une barre centrale qui ne soit pas �tir�e, utilisez :
  Bar_L.gif, Bar_ML.gif, Bar_M.gif, Bar_MR.gif, Bar_R.gif
Dans ce cas, Bar_ML.gif et Bar_MR.gif ne seront pas �tir�es (voir le th�me
Craftsman pour un exemple de cette technique).

<b>Locked.gif</b>
Une image de cadenas pour tous les r�pertoires ayant un fichier nomm�
.htaccess.

<b>Images de fond</b>
Si vous voulez une image en fond d'�cran, sp�cifiez-la dans le fichier Font
dans la section $BODY comme dans l'exemple ci-dessus. La valeur de la variable
$PATH sera remplac�e par le chemin d'acc�s aux fichiers du th�me.

<b>More.gif</b>
Quand une page d'album comporte plusieurs sous-albums � lister, l'image
'More.gif' est affich�e en premier.

<b>Next.gif, Prev.gif, Back.gif</b>
Ces images sont respectivement utilis�es pour les boutons des fl�ches arri�re
et avant et pour le bouton pour remonter dans la hi�rarchie des albums.

<b>Icon.gif</b>
Cette image, affich�e dans le coin sup�rieur gauche des albums parents, est
souvent le texte "Photos" seulement.

<b>Images des bordures</b>
Il y a plusieurs m�thodes pour d�couper une bordure, de la plus simple � la
plus complexe, l'ensemble d�pendant de la complexit� de la bordure et du
nombre de sections que vous voulez pouvoir �tirer :

  <b>4 morceaux</b>  Utilisez  Bord_L.gif, Bord_R.gif, Bord_T.gif, Bord_B.gif
    (les coins vont avec les images droites et gauches)
    Les bordures en 4 morceaux ne s'�tirent en g�n�ral pas tr�s bien et de
    fait ne fonctionnent pas bien sur les pages d'images. En g�n�ral, vous
    pouvez couper les coins des images droites et gauches pour faire :

  <b>8 morceaux</b>  Incluez aussi Bord_TL.gif, Bord_TR.gif, Bord_BL.gif,
    Bord_BR.gif
    Les bordures en 8 morceaux vous permettent g�n�ralement d'�tirer et de
    g�rer la plupart des images recadr�es.

  <b>12 morceaux</b>  Incluez aussi Bord_LT.gif, Bord_RT.gif, Bord_LB.gif,
    Bord_RB.gif
    12 morceaux permettent d'�tirer des bordures ayant des coins plus
    complexes comme par exemple les th�mes Dominatrix ou Ivy.

  <b>Bordures chevauchantes</b>  Peuvent utiliser des images transparentes qui
  peuvent partiellement chevaucher vos vignettes. Voir ci-dessous.

Voici comment des morceaux pour des bordures normales sont dispos�s :

 bordure � 12 morceaux
      TL  T  TR      bordure � 8 morceaux    bordure � 4 morceaux
      LT     RT            TL  T  TR            TTTTTTT
      L  IMG  R            L  IMG  R            L IMG R
      LB     RB            BL  B  BR            BBBBBBB
      BL  B  BR

Notez que chaque rang�e d'images doit avoir la m�me hauteur, alors que ce
<b>n'est pas</b> le cas pour la largeur (c'est-�-dire hauteur TL = hauteur T =
hauteur TR).
Ceci n'est pas totalement vrai pour les bordures qui se chevauchent !

Une fois que vous avez cr�� ces fichiers, vous pouvez lancer l'utilitaire
simmer_theme (un outil t�l�chargeable depuis MarginalHacks.com) et votre th�me
deviendra pr�t � �tre utilis� !

Si vous avez besoin de bordures diff�rentes pour les pages d'images, alors
utilisez les m�mes noms de fichiers maix en les pr�fixant par 'I' comme par
exemple IBord_LT.gif, IBord_RT.gif,...

Les bordures chevauchantes autorisent des effets r�ellement fantastiques avec
les bordures des images.  Actuellement, il n'y a pas de support pour des
bordures chevauchantes diff�rentes pour les images.

Pour utiliser les bordures chevauchantes, cr�ez les images :
      Over_TL.png   Over_T.png   Over_TR.png
      Over_L.png                 Over_R.png
      Over_BL.png   Over_B.png   Over_BR.png

Puis, d�terminez combien de pixels des bordures vont d�border (en dehors de la
photo). Mettez ces valeurs dans des fichiers: Over_T.pad Over_R.pad Over_B.pad
Over_L.pad.
Voir le th�me "Themes/FunLand" pour un exemple simple.

<b>Images polyglottes</b>
Si vos images comportent du texte, vous pouvez les traduire dans d'autres
langues et, ainsi, vos albums pourront �tre g�n�r�s dans d'autres
langages. Par exemple, vous pouvez cr�er une image "More.gif" hollandaise et
mettre ce fichier dans le r�pertoire 'lang/nl' de votre th�me (nl est le code
de langage pour le hollandais).
Quand l'utilisateur lancera album en hollandais (album -lang nl) alors le
th�me utilisera les images hollandaises s'il les trouve. Le th�me "Themes/Blue"
comporte un tel exemple simple. Les images actuellement traduites sont :
  More, Back, Next, Prev and Icon

Vous pouvez cr�er une page HTML qui donne la liste des traductions
imm�diatement disponibles dans les th�mes avec l'option -list_html_trans :

<code>% album -list_html_trans > trans.html</code>

Puis, visualisez le fichier trans.html dans un navigateur. Malheureusement,
plusieurs langages ont des tables d'encodage des caract�res diff�rents et une
page HTML n'en dispose que d'une seule. La table d'encodage est utf-8 mais
vous pouvez �diter la ligne "charset=utf-8" pour visualiser correctement les
diff�rents caract�res utilis�s dans les langages en fonction des diff�rentes
tables d'encodage (comme par exemple l'h�breu).

Si vous avez besoin de plus de mots traduits pour les th�mes, faites-le moi
savoir et si vous cr�ez des images dans une nouvelle langue pour un th�me,
envoyez-les moi s'il vous pla�t !


ITEM: Partir de z�ro : l'API des th�mes

Ceci est une autre paire de manches : vous avez besoin d'avoir une id�e
vraiment tr�s claire de la fa�on dont vous voulez qu'album positionne vos
images sur votre page. Ce serait peut-�tre une bonne id�e de commencer en
premier par modifier des th�mes existants afin de cerner ce que la plupart des
th�mes fait. Regardez �galement les th�mes existants pour des exemples de code
(voir les suggestions ci-dessus).

Les th�mes sont des r�pertoires qui contiennent au minimum un fichier
'album.th'. Ceci est le patron du th�me pour les pages de l'album. Souvent,
les r�pertoires contiennent aussi un fichier 'image.th' qui est un patron du
th�me pour les images ainsi que des fichiers graphiques / css utilis�s par le
th�me. Les pages de l'album contiennent les vignettes et les pages des images
montrent les images pleine page ou recadr�es.

Les fichiers .th sont en ePerl qui est du perl encapsul� � l'int�rieur
d'HTML. Ils finissent par ressembler � ce que sera l'album et les images HTML
eux-m�mes.

Pour �crire un th�me, vous aurez besoin de conna�tre la syntaxe ePerl. Vous
pouvez trouver plus d'information sur l'outil ePerl en g�n�ral � MarginalHacks
(bien que cet outil ne soit pas n�cessaire pour l'utilisation des th�mes dans album).

Il y a une pl�thore de routines de support disponibles mais souvent une
fraction d'entre elles seulement est n�cessaire (cela peut aider de regarder
les autres th�mes afin de voir comment ils sont construits en g�n�ral).

Table des fonctions:

<b>Fonctions n�cessaires</b>
Meta()                    Doit �tre appel�e depuis la section <head> du HTML
Credit()                  Affiche le cr�dit ('cet album a �t� cr�� par..')
                          Doit �tre appel�e depuis la section <body> du HTML
Body_Tag()                Appel�e depuis l'int�rieur du tag <body>.

<b>Chemins et options</b>
Option($name)             Retourne la valeur d'une option / configuration
Version()                 Retourne la version d'album
Version_Num()             Retourne le num�ro de la version d'album en chiffres
                            uniquement (c'est-�-dire. "3.14")
Path($type)               Retourne l'information du chemin pour le $type de :
  album_name                Nom de l'album
  dir                       R�pertoire courant de travail d'album
  album_file                Chemin complet au fichier d'album index.html
  album_path                Chemin des r�pertoires parent
  theme                     Chemin complet du r�pertoire du th�me
  img_theme                 Chemin complet du r�pertoire du th�me depuis les pages des images
  page_post_url             Le ".html" � ajouter sur les pages des images
  parent_albums             Tableau des albums parents (segmentation par album_path)
Image_Page()              1 si on est en train de g�n�rer une page d'image, 0 si c'est une page de vignettes
Page_Type()               Soit 'image_page' ou 'album_page'
Theme_Path()              Le chemin d'acc�s (via le syst�me de fichiers) aux fichiers du th�me
Theme_URL()               Le chemin d'acc�s (via une URL) aux fichiers du th�me

<b>En-t�te et pied-de-page</b>
isHeader(), pHeader()     Test pour et afficher l'en-t�te
isFooter(), pFooter()     La m�me chose pour le pied-de-page

En g�n�ral, vous bouclez � travers les images et les r�pertoires en utilisant
des variables locales :

  my $image = First('pics');
  while ($image) {
    print Name($image);
    $image = Next($image);
  }

Ceci affichera le nom de chaque image. Nos types d'objets sont soit 'pics'
pour les images soit 'dirs' pour les r�pertoires-enfants. Ici se trouvent les
fonctions relatives aux objets :

<b>Objets</b> (le type est soit 'pics' (d�faut) soit 'dirs')
First($type)             Renvoie comme objet la premi�re image ou le premier sous-album.
Last($type)              Renvoie le dernier objet
Next($obj)               Etant donn� un objet, renvoie l'objet suivant
Next($obj,1)             La m�me chose mais retourne au d�but une fois la fin atteinte
Prev($obj), Prev($obj,1) Similaire � Next()

num('pics') ou juste num() Nombre total d'images de cet album
num('dirs')                Nombre total de sous-albums
Egalement :
num('parent_albums')       Nombre total d'albums parents conduisant � cet album

Consultation d'objet :
get_obj($num, $type, $loop)
                          Trouve un objet � partir de son num�ro (type 'pics' ou 'dirs').
                          Si la variable $loop est positionn�e alors le
			  compteur rebouclera au d�but lors de la recherche

<b>Propri�t�s des objets</b>
Chaque objet (image ou sous-album) poss�de des propri�t�s.
L'acc�s � certaines propri�t�s se fait par un simple champ :

Get($obj,$field)            Retourne un simple champ pour un objet donn�

Champ                     Description
-----                     ----------
type                      Quel type d'objet ? Soit 'pics' soit 'dirs'
is_movie                  Bool�en: est-ce que c'est un film ?
name                      Nom (nettoy� et optionnel pour les l�gendes)
cap                       L�gende de l'image
capfile                   Fichier optionnel des l�gendes
alt                       Etiquette (tag) alt
num_pics                  [r�pertoires seulement, -dir_thumbs] Nombre d'images dans le r�pertoire
num_dirs                  [r�pertoires seulement, -dir_thumbs] Nombre de sous-r�pertoire dans le r�pertoire

L'acc�s � des propri�t�s se fait via un champ et un sous-champ. Par exemple,
chaque image comporte une information sur ses diff�rentes tailles : plein
�cran, moyenne et vignette (bien que la taille moyenne soit optionnelle).

Get($obj,$size,$field)      Retourne la propri�t� de l'image pour une taille donn�ee

Taille        Champ       Description
------        -----       ----------
$size         x           Largeur
$size         y           Hauteur
$size         file        Nom du fichier (sans le chemin)
$size         path        Nom du fichier (chemin complete)
$size         filesize    Taille du fichier en octets
full          tag         L'�tiquette (tag) (soit 'image' soit 'embed') - seulement pour 'full'

Il y a aussi des informations relatives aux URL dont l'acc�s se fait en
fonction de la page d'o� on vient ('from' / depuis) et l'image ou la page vers
lequel on va ('to' / vers) :

Get($obj,'URL',$from,$to)   Renvoie un URL pour un objet 'depuis -> 'vers
Get($obj,'href',$from,$to)  Idem mais utilise une cha�ne de caract�res avec 'href'
Get($obj,'link',$from,$to)  Idem mais met le nom de l'objet � l'int�rieur d'un lien href

Depuis       Vers         Description
------       ----         ----------
album_page   image        Image_URL vers album_page
album_page   thumb        Thumbnail vers album_page
image_page   image        Image_URL vers image_page
image_page   image_page   Cette page d'image vers une autre page d'image
image_page   image_src    L'URL d'&lt;img src&gt; pour la page d'image
image_page   thumb        Page des vignettes depuis la page d'image

Les objets r�pertoires ont aussi :

Depuis       Vers         Description
------       ----         ----------
album_page   dir          URL vers le r�pertoire depuis la page de son album-parent


<b>Albums parent</b>
Parent_Album($num)        Renvoie en cha�ne de caract�res le nom de l'album parent (incluant le href)
Parent_Albums()           Retourne une liste de cha�nes de caract�res des albums parents (incluant le href)
Parent_Album($str)        Cr�e la cha�ne de caract�res $str � partir de l'appel  � la fonction Parent_Albums()
Back()                    L'URL pour revenir en arri�re ou remonter d'une page

<b>Images</b>
This_Image                L'objet image pour une page d'image
Image($img,$type)         Les �tiquettes d'&lt;img&gt; pour les types 'medium', 'full' et 'thumb'
Image($num,$type)         Idem mais avec le num�ro de l'image
Name($img)                Le nom nettoy� ou l�gend� pour une image
Caption($img)             La l�gende pour une image

<b>Albums enfants</b>
Name($alb)		  Le nom du sous-album
Caption($img)             La l�gende pour une image



<b>Quelques routines utiles</b>
Pretty($str,$html,$lines) Formate un nom.
    Si la variable $html est d�finie alors le code HTML est autoris�
    (c'est-�-dire utilise 0 pour &lt;title&gt; et associ�s). Actuellement,
    pr�fixe seulement avec la date dans une taille de caract�res plus petite
    (c'est-�-dire '2004-12-03.Folder').
    Si la variable $lines est d�finie alors le multiligne est
    autoris�. Actuellement, suis seulement la date avec un retour � la ligne 'br'.
New_Row($obj,$cols,$off)  Devons-nous commencer une nouvelle ligne apr�s cet objet ?
			  Utiliser la variable $off si l'offset du premier objet d�marre � partir de 1
Image_Array($src,$x,$y,$also,$alt)
                          Retourne un tag HTML &lt;img&gt; � partir de $src, $x,...
Image_Ref($ref,$also,$alt)
			  Identique � Image_Array, mais la variable $ref peut
			  �tre une table de hachage de Image_Arrays index�e par
			  le langage (par d�faut, '_').
			  album choisit l'Image_Array en fonction de la d�finition des langages.
Border($img,$type,$href,@border)
Border($str,$x,$y,@border)
                          Cr�e une image enti�re entour�e de bordures. Voir
			  'Bordures' pour de plus amples d�tails.


Si vous cr�ez un th�me � partir de z�ro, consid�rez l'ajout du support pour
l'option -slideshow. Le moyen le plus facile de r�aliser cette op�ration est
de copier / coller le code "slideshow" d'un th�me existant.


ITEM: Soumettre des th�mes

Vous avez personnalis� un th�me ? Je serais ravi de le voir m�me s'il est
totalement sp�cifique � votre site internet.

Si vous avez un nouveau th�me que vous souhaiteriez rendre publique,
envoyez-le moi ou envoyez une adresse URL o� je puisse le voir. N'oubliez pas
de mettre le fichier CREDIT � jour et faites-moi savoir si vous donnez ce
th�me � MarginalHacks pour une utilisation libre ou si vous souhaitez le
mettre sous une license particuli�re.




ITEM: Conversion des th�mes v2.0 aux th�mes v3.0

La version v2.0 d'album a introduit une interface de th�mes qui a �t� r��crite
dans la version v3.0 d'album. La plupart des th�mes de la version 2.0 (plus
sp�cialement ceux qui n'utilisent pas la plupart des variables globales)
fonctionneront toujours mais l'interface est obsol�te et pourrait dispara�tre
dans un futur proche.

La conversion des th�mes de la version 2.0 � la version 3.0 n'est pas bien
difficile.

Les th�mes de la version 2.0 utilisent des variables globales pour tout un tas
de choses. Celles-ci sont maintenant obsol�tes. Dans la version 3.0, vous
devez conserver une trace des images et des r�pertoires dans des variables et
utiliser des 'it�rateurs' qui sont fournis. Par exemple :

  my $image = First('pics');
  while ($image) {
    print Name($image);
    $image = Next($image);
  }

Ceci parcourra toutes les images et affichera leur nom.
Le tableau ci-dessous vous montre comment r��crire des appels cod�s avec
l'ancien style utilisant une variable globale en des appels cod�s avec le
nouveau style qui utilise une variable locale. Cependant pour utiliser ces
appels, vous devez modifier vos boucles comme ci-dessus.

Voici un tableau de conversion pour aider au passage des th�mes de la version
2.0 � la version 3.0 :

# Les variables globales ne doivent plus �tre utilis�es
# OBSOLETE - Voir les nouvelles m�thodes d'it�rations avec variables locales
# ci-dessus
$PARENT_ALBUM_CNT             R��crire avec : Parent_Album($num) et Parent_Albums($join)
$CHILD_ALBUM_CNT              R��crire avec : my $dir = First('dirs');  $dir=Next($dir);
$IMAGE_CNT                    R��crire avec : my $img = First('pics');  $pics=Next($pics);
@PARENT_ALBUMS                Utiliser � la place : @{Path('parent_albums')}
@CHILD_ALBUMS, @CHILD_ALBUM_NAMES, @CHILD_ALBUM_URLS, ...

# Anciennes m�thodes modifiant les variables globales
# OBSOLETE - Voir les nouvelles m�thodes d'it�rations avec variables locales
# ci-dessus
Next_Image(), Images_Left(), Image_Cnt(), Image_Prev(), Image_Next()
Set_Image_Prev(), Set_Image_Next(), Set_Image_This()
Next_Child_Album(), Child_Album_Cnt(), Child_Albums_Left()

# chemins et autres
pAlbum_Name()                 Path('album_name')
Album_Filename()              Path('album_file')
pFile($file)                  print read_file($file)
Get_Opt($option)              Option($option)
Index()                       Option('index')
pParent_Album($str)           print Parent_Album($str)

# Albums parents
Parent_Albums_Left            Obsol�te, voir '$PARENT_ALBUM_CNT' ci-dessus
Parent_Album_Cnt              Obsol�te, voir '$PARENT_ALBUM_CNT' ci-dessus
Next_Parent_Album             Obsol�te, voir '$PARENT_ALBUM_CNT' ci-dessus
pJoin_Parent_Albums           print Parent_Albums(\@_)

# Images, utilisant la variable '$img'
pImage()                      Utiliser $img � la place et :
                              print Get($img,'href','image');
                              print Get($img,'thumb') if Get($img,'thumb');
                              print Name($img), "&lt;/a&gt;";
pImage_Src()                  print Image($img,'full')
Image_Src()                   Image($img,'full')
pImage_Thumb_Src()            print Image($img,'thumb')
Image_Name()                  Name($img)
Image_Caption()               Caption($img)
pImage_Caption()              print Get($img,'Caption')
Image_Thumb()                 Get($img,'URL','thumb')
Image_Is_Pic()                Get($img,'thumb')
Image_Alt()                   Get($img,'alt')
Image_Filesize()              Get($img,'full','filesize')
Image_Path()                  Get($img,'full','path')
Image_Width()                 Get($img,'medium','x') || Get($img,'full','x')
Image_Height()                Get($img,'medium','y') || Get($img,'full','y')
Image_Filename()              Get($img,'full','file')
Image_Tag()                   Get($img,'full','tag')
Image_URL()                   Get($img,'URL','image')
Image_Page_URL()              Get($img,'URL','image_page','image_page') || Back()

# Routines pour les albums enfant utilisant la variable '$alb'
pChild_Album($nobr)           print Get($alb,'href','dir');
                              my $name = Name($alb);
                              $name =~ s/&lt;br&gt;//g if $nobr;
                              print $name,"&lt;/a&gt;";
Child_Album_Caption()         Caption($alb,'dirs')
pChild_Album_Caption()        print Child_Album_Caption($alb)
Child_Album_URL()             Get($alb,'URL','dir')
Child_Album_Name()            Name($alb)

# Inchang�
Meta()                        Meta()
Credit()                      Credit()

ITEM: Traduit par:

Jean-Marc [jean-marc.bouche AT 9online.fr]
</span>
