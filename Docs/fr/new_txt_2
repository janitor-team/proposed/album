MINI HOW-TO

<span lang="fr">
ITEM: Simple album

En supposant que vous avez d�j� install� album correctement, nous pouvons
r�aliser quelques manipulations basiques. Si vous rencontrez une erreur ou
un quelconque probl�me ici, regardez les documents d'installation.

Vous avez besoin d'un r�pertoire accessible depuis le web o� mettre vos
th�mes et votre album photos. Nous utiliserons /home/httpd/test dans ce
document. Ce r�pertoire a besoin d'�tre accessible par le serveur web. Dans
cet exemple, nous utiliserons l'URL suivante :
	http://myserver/test

Adaptez vos commandes / URLs en fonction de vos besoins.

Premi�rement, cr�ez un r�pertoire et mettez-y des images dedans. Nous
l'appellerons ainsi :
	/home/httpd/test/Photos

Puis nous ajouterons quelques images d�nomm�es 'IMG_001.jpg' � 'IMG_004.jpg'.

Maintenant, pour ce test basique, lançons simplement album:

<code>% album /home/httpd/test/Photos</code>

Maintenant, vous pouvez visualiser l'album cr�� via un navigateur � l'adresse :
	http://myserver/test/Photos

ITEM: Ajouter des l�gendes

Cr�ez un fichier /home/httpd/test/Photos/captions.txt avec le contenu
suivants (utilisez la touche 'tab' si vous voyez "  [tab]  ") :

-- captions.txt ---------
IMG_001.jpg  [tab]  Nom de la premi&egrave;re image
IMG_002.jpg  [tab]  Deuxi&egrave;me image
IMG_003.jpg  [tab]  Encore une image      [tab] avec une l&eacute;gende!
IMG_004.jpg  [tab]  Derni&egrave;re image        [tab] avec une autre l&eacute;gende.
-------------------------

Puis lancer de nouveau la commande album :

<code>% album /home/httpd/test/Photos</code>

Vous verrez que la l�gende a �t� modif�e.

Maintenant, cr�ez un fichier avec du texte dedans :
/home/httpd/test/Photos/header.txt

Et relancez la commande album une fois de plus. Vous verrez alors le texte
du fichier affich�e au sommet de la page.

ITEM: Masquer des photos

Il y a quelques moyens de masquer des photos / des fichiers / des r�pertoires
mais nous allons utiliser le fichier des l�gendes. Essayez de placer un
commentaire avec le caract�re '#' devant le nom d'une image dans captions.txt:

-- captions.txt ---------
IMG_001.jpg  [tab]  Nom de la premi&egrave;re image
#IMG_002.jpg  [tab]  Deuxi&egrave;me image
IMG_003.jpg  [tab]  Encore une image      [tab] avec une l&eacute;gende!
IMG_004.jpg  [tab]  Derni&egrave;re image        [tab] avec une autre l&eacute;gende.
-------------------------

Relancez la commande album et vous verrez que l'image IMG_002.jpg a maintenant
disparu.
Si vous aviez proc�d� de la sorte la premi�re fois, vous n'auriez g�n�r� ni
l'image de taille r�duite ni la vignette correspondante. Si vous le souhaitez,
vous pouvez les supprimer en utilisant l'option -clean :

<code>% album -clean /home/httpd/test/Photos</code>

ITEM: Utiliser un th�me

Si les th�mes ont �t� correctement install�s and sont accessibles via
them_path (chemin d'acc�s aux th�mes), alors vous pouvez utiliser un th�me
lors de la g�n�ration de votre album :

<code>% album -theme Blue /home/httpd/test/Photos</code>

L'album photo courant devrait �tre maintenant bas� sur le th�me Blue (bleu).
S'il y a tout un tas de liens cass�s ou d'images qui n'apparaissent pas, il
y a de forte chance pour que votre th�me n'ait pas �t� install� dans un
r�pertoire accessible depuis le web ; voir les documents d'installation.

Album sauvegarde les options que vous lui indiquez. Ainsi, la prochaine fois
que vous lancerez la commande album :

<code>% album /home/httpd/test/Photos</code>

vous utiliserez toujours le th�me Blue. Pour d�sactiver un th�me, tapez :

<code>% album -no_theme /home/httpd/test/Photos</code>


ITEM: Images r�duites

Les images de pleine taille sont en g�n�rale d'une taille trop imposante pour
un album photo sur le web. C'est pourquoi nous utiliserons des images r�duites
sur les pages web g�n�r�es :

<code>% album -medium 33% /home/httpd/test/Photos</code>

Cependant, vous avez toujours acc�s aux images pleine taille en cliquant
simplement sur les images r�duites.

La commande :

<code>% album -just_medium /home/httpd/test/Photos</code>

emp�chera la liaison entre les images de taille r�duite et les images pleine
taille, en pr�sumant que nous avons lanc� pr�c�demment une commande avec
l'option -medium.

ITEM: Ajouter des l�gendes EXIF

Ajoutons la valeur du diaphragme dans les l�gendes de chaque image.

<code>% album -exif "&lt;br&gt;diaphragme=%Aperture%" /home/httpd/test/Photos

Cette commande ajoutera seulement la valeur du diaphragme pour les images qui
disposent de la balise exif (le symbole entre les caract�res '%') 'Aperture'
signifiant diaphragme en fran�ais ! Nous avons �galement ajout� la balise
&lt;br&gt; qui permet d'ajouter cette information exif sur une nouvelle ligne.

Nous pouvons rajouter d'autres informations exif :

<code>% album -exif "&lt;br&gt;focale: %FocalLength%" /home/httpd/test/Photos

Parce que album sauvegarde vos pr�c�dentes options, nous disposons maintenant
de deux balises exif pour toutes les images sp�cifiant � la fois le diaphragme
et la focale.
Supprimons le diaphragme :

<code>% album -no_exif "&lt;br&gt;diaphragme=%Aperture%" /home/httpd/test/Photos</code>

La cha�ne de caract�res suivant l'option '-no_exif' a besoin de correspondre
exactement � celle entr�e pr�c�demment avec l'option '-exif' sinon elle sera
ignor�e. Vous pouvez �galement �diter le fichier de configuration qu'album a
cr�� ici :
	/home/httpd/test/Photos/album.conf
Puis supprimez l'option en question.

ITEM: Ajouter d'autres albums

Imaginons que vous faites un voyage en Espagne. Vous prenez des photos et les
mettez dans le r�pertoire :
	/home/httpd/test/Photos/Espagne/

Maintenant, ex�cutez la commande album depuis le r�pertoire principal :

<code>% album /home/httpd/test/Photos</code>

Cette commande modifiera l'album principal Photos qui sera reli� � Espagne
puis elle lancera �galement la commande album sur le r�pertoire Espagne avec
les m�mes caract�ristiques / th�me, etc.

Maintenant, faisons un autre voyage en Italie et cr�ons le r�pertoire :

	/home/httpd/test/Photos/Italie/

Nous pourrions lancer la commande depuis le r�pertoire principal :

<code>% album /home/httpd/test/Photos</code>

Mais ceci rescannerait le r�pertoire Espagne qui n'a pas chang�. Album ne
g�n�rera aucune page HTML ni vignette � moins qu'il ait le besoin de la
faire. Cependant, il peut perdre du temps, plus particuli�rement si vos
albums photos deviennent de plus en plus volumineux. Aussi, vous pouvez
juste demander d'ajouter le nouveau r�pertoire :

<code>% album -add /home/httpd/test/Photos/Italie</code>

Cette commande modifiera l'index de la premi�re page (dans Photos) et
g�n�rera l'album correspond au r�pertorie Italie.

ITEM: Traduit par:

Jean-Marc [jean-marc.bouche AT 9online.fr]
</span>
