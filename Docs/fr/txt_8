Support des langues

<span lang="fr">
ITEM: Utilisation des langues
(nécessite album v4.0 ou supérieur)

Album est fourni avec des fichiers de langues (nous avons besoin d'une aide
pour les traductions ; voyez ci-dessous !). Les fichiers de langues modifieront
la plupart des messages envoyés par album, de même que toutes les sorties HTML
qui ne sont pas générés par le thème.

Modifier tout le texte d'un thème pour qu'il corresponde à votre langue est
aussi simple qu'éditer les fichiers du thème. Il y a un exemple de ceci avec
le thème "simple-Czech".

Pour utiliser une langue, ajoutez l'option "lang" à votre fichier principal
album.conf ou autrement, spécifiez-la lorsque vous générez votre album pour la
première fois (elle sera sauvée pour cet album par la suite).

Les langues peuvent être effacées comme n'importe quelle option avec :

% album -clear_lang ...

Vous pouvez spécifier plusieurs langues, ce qui peut être utile si une langue
est incomplète, mais vous voudriez une langue par défaut autre que
l'anglais. Ainsi, par exemple, si vous voulez le hollandais comme langue
principal avec en secours le suédois chef [sic !], vous pourriez faire :

% album -lang swedish_chef -lang nl ...

Si vous spécifiez un sous-langage (tel que es-bo pour espagnol, Bolivie),
alors album essayera 'es' en premier comme langue de secours.

Notez qu'à cause d'un bogue connu, vous devez spécifier toutes les langues
désirées en un seul coup plutôt qu'au cours de plusieurs invocations d'album.

Pour voir quelles sont les langues disponibles :

% album -list_langs

Malheureusement, la plupart des langues sont tout juste complètes mais ceci
est une chance pour les personnes qui souhaiteraient nous aider pour devenir un...

ITEM: Traducteurs volontaires

Le projet album a besoin de volontaires pour réaliser des traductions
principalement :

1) Le Mini How-To. Traduisez s'il vous plaît à partir de la <a href="txt_2">source</a>.
2) les messages d'album comme expliqué ci-dessous.

Si vous êtes disposés à traduire l'une ou les deux sections, contactez-moi
s'il vous plaît !

Si vous vous sentez particulièrement ambitieux, n'hésitez pas à traduire
n'importe quelle section de la documentation en me le faisant savoir !


ITEM: Traduction de la documentation

Le document le plus important à traduire est le <a href="Section_2.html">"Mini How-To"</a>.
Traduisez-le s'il vous plaît à partir du <a href="txt_2">texte original</a>.

Veuillez également me faire savoir comment vous souhaitez être remercié, par
votre nom, un nom et une adresse électronique ou un nom et un site internet.

Egalement, s'il vous plaît, incluez l'orthographe et la distinction majuscule
/ minuscule adéquates du nom de votre langue <b>dans votre langue</b>. Par
exemple, "français" à la place de "French".

Je suis ouvert à toute suggestion concernant le jeu de caractères à
utiliser. Les options actuelles sont :

1) Caractères HTML tel que [&amp;eacute;]] (bien qu'ils ne fonctionnent que
dans les navigateurs)
2) Unicode (UTF-8) tel que [Ã©] (seulement dans les navigateurs et dans
quelques terminaux)
3) Code ASCII, si possible, tel que [é] (fonctionne dans les éditeurs de
texte mais pas dans cette page configurée avec le jeu de caractères unicode)
4) Code iso spécifique à certaines langues comme le jeu iso-8859-8-I pour l'hébreu.

Actuellement, le jeu unicode (utf-8) semble être le mieux placé pour les
langues qui ne sont pas couvertes par le jeu iso-8859-1 car il couvre toutes
les langues et nous aide à gérer les tradutions incomplètes (qui autrement
nécessiteraient de multiples jeux de caractères ce qui serait un
désastre). N'importe quel code iso qui est un sous-ensemble de l'utf-8 peut
être utilisé.

Si le terminal principal pour une langue donnée a un jeu de caractères iso à
la place de l'utf, alors cela peut être une bonne raison d'utiliser un jeu de
caractères non-utf.


ITEM: Messages d'Album

album gère tous les textes de messages en utilisant son propre système de
support de langue, similaire au système utilisé par le module perl <a href="/redir.cgi?search.cpan.org/~petdance/Locale-Maketext/lib/Locale/Maketext.pod">Locale::Maketext</a>.
(plus d'information sur l'inspiration du système dans <a href="/redir.cgi?search.cpan.org/~petdance/Locale-Maketext-1.10/lib/Locale/Maketext/TPJ13.pod">TPJ13</a>)

Un message d'erreur, par exemple, peut ressembler à ceci :

  No themes found in [[some directory]].

Avec un exemple spécifique devenir :

  No themes found in /www/var/themes.

En hollandais, ceci donnerait :

  Geen thema gevonden in /www/var/themes.

La "variable" dans ce cas est "/www/var/themes" et n'est évidemment pas
traduite. Dans album, le vrai message d'erreur ressemble à cela :

  'No themes found in [_1].'
  # Args:  [_1] = $dir

La traduction (en hollandais) ressemble à ceci :

  'No themes found in [_1].' => 'Geen thema gevonden in [_1].'

Après la traduction, album remplacera [_1] par le nom du répertoire.

Il y a parfois plusieurs variables pouvant changer de position :

Quelques exemples de messages d'erreur :

  Need to specify -medium with -just_medium option.
  Need to specify -theme_url with -theme option.

En hollandais, le premier donnerait :

  Met de optie -just_medium moet -medium opgegeven worden.

Le message d'erreur réel avec sa traduction en hollandais est :

  'Need to specify [_1] with [_2] option'
  => 'Met de optie [_2] moet [_1] opgegeven worden'
  # Args: [_1]='-medium'  [_2]='-just_medium'

Note que les variables ont changé de position.

Il y a aussi des opérateurs spéciaux pour les quantités. Par exemple, nous
souhaitons traduire :

  'I have 42 images'

ou le nombre 42 peut changer. En anglais, il est correct de dire :

  0 images, 1 image, 2 images, 3 images...

alors qu'en hollandais nous aurons :

  0 afbeeldingen, 1 afbeelding, 2 afbeeldingen, 3 afbeeldingen..

Mais d'autres langues (telles que des langues slaves) peuvent avoir des règles
spéciales quant à savoir si "image" doit être au pluriel en fonction de la
quantité demandée 1, 2, 3 ou 4 etc ! Le cas le plus simple est couvert par
l'opérateur [quant] :

  [quant,_1,image]

Ceci est similaire à "[_1] image" excepté que "image" sera mis au pluriel si
[_1] est 0 ou plus grand que 1 :

  0 images, 1 image, 2 images, 3 images...

La forme plurielle s'obtient simplement en ajoutant un 's'. Si cela n'est pas
correct, nous pouvons spécifier la forme plurielle :

  [quant,_1,afbeelding,afbeeldingen]

qui nous donne le décompte en hollandais évoqué plus haut.

Et si nous avons besoin d'une forme spécifique pour 0, nous pouvons le
spécifier :

  [quant,_1,directory,directories,no directories]

ce qui donnerait :

  no directories, 1 directory, 2 directories, ...

Il y a aussi un raccourci pour [quant] en utilisant '*' d'où les équivalences
:

  [quant,_1,image]
  [*,_1,image]

Finalement, voici un exemple de traduction pour un nombre d'images :

  '[*,_1,image]'
  => '[*,_1,afbeelding,afbeeldingen]',

Si vous avez quelque chose de plus compliqué alors vous pouvez utiliser du
code perl. Je peux vous aider à l'écrire si vous m'indiquez comment la
traduction doit fonctionner :

  '[*,_1,image]'
  => \&russian_quantity;    # Ceci est une sous-routine définie quelque part

Puisque les chaînes traduites sont (généralement) placées entre des
apostrophes (') et aussi à cause des codes spéciaux entre [crochets], nous
avons besoin de les citer correctement.

Les apostrophes dans une chaîne ont besoin d'être précédées par une barre
oblique ou slash en anglais (\) :

  'I can\'t find any images'

et les crochets sont cités en utilisant le tilda (~) :

  'Problem with option ~[-medium~]'

ce qui malheureusement peut devenir assez déplaisant si la chose à l'intérieur
des crochets est une variable :

  'Problem with option ~[[_1]~]'

Soyez prudent et vérifiez que tous les crochets sont fermés de façon
appropriée.

De plus, dans presque tous les cas, la traduction devrait avoir le même nombre
de variables que le message original :

  'Need to specify [_1] with [_2] option'
  => 'Met de optie [_2] moet'              # <- Où est passé [_1] ?!?


Heureusement, la plupart du travail est faite pour vous. Les fichiers de
langue sont sauvés dans le répertoire spécifié par l'option -data_path (ou
-lang_path) où album stocke ses données. Ce sont essentiellement du code perl
et ils peuvent être auto-générés par album :

% album -make_lang sw

Cette commande créera un nouveau fichier de langue vide dénommé'sw'
(suédois). Allez de l'avant en éditant ce fichier et en ajoutant autant de
traductions que vous pouvez. Les traductions laissées vides sont tolérées :
elles ne seront simplement pas utilisées. Les traductions parmi les plus
importantes (comme celles qui sont affichées dans les pages HTML) se trouvent
au sommet du fichier et devraient probablement être traduite en premier.

Choisir un jeu de caractères n'est pas chose aisée car il devrait être basé
sur les jeux de caractères que vous pensez que les gens sont susceptibles
d'avoir à disposition aussi bien dans leur terminal que dans leur navigateur.

Si vous voulez construire un nouveau fichier de langue en utilisant une
traduction provenant d'un fichier existant (par exemple pour mettre à jour une
langue avec les nouveaux messages qui ont été ajoutés dans album), vous devez
d'abord chargé la langue en premier :

% album -lang sw -make_lang sw

Toutes les traductions dans le fichier de langue suédois courant seront
copiées vers le nouveau fichier (quoique les commentaires et autre code ne
soient pas copiés).

Pour les très longues lignes de texte, ne vous faites pas de souci en ajoutant
des caractères de fin de ligne (\n) excepté s'ils existent dans le message
original : album gérera de façon appropriée les sections de texte les plus longues.

Contactez-moi s'il vous plaît quand vous commencez un travail de
traduction. Ainsi je peux être sûre que nous n'avons pas deux traducteurs
travaillant sur les mêmes parties. Et soyez sûre de m'envoyer des mises à jour
des fichiers de langue au fur et à mesure des progrès ; même s'ils sont
incomplets, ils sont utiles !

Si vous avez des questions à propos de la façon de lire la syntaxe des
fichiers de langue, faites-le moi savoir s'il vous plaît.


ITEM: Pourquoi vois-je toujours des termes anglais ?

Après avoir chois une autre langue, vous pouvez toujours parfois voir des
termes en anglais :

1) Les noms des options sont toujours en anglais. (-geometry est toujours
   -geometry)
2) La chaîne courante n'est actuellement pas traduite.
3) Il est peu probable que la sortie d'un module plug-in soit traduite.
4) Les fichiers de langue ne sont pas toujours complet et ne traduiront
uniquement que ce qu'ils connaissent.
5) album peut avoir de nouveaux messages que le fichier de langue ne connaît
encore pas.
6) La plupart des thèmes sont en anglais.

Heureusement, ce dernier est le plus simple à changer. Editer simplement le
thème et remplacer les portions de texte HTML avec n'importe quelle langue que
vous souhaitez ou créez de nouveau graphique dans une langue différentes pour
les icônes en anglais.
Si vous créez un nouveau thème, je serais ravi de le savoir !

ITEM: Traduit par:

Jean-Marc [jean-marc.bouche AT 9online.fr]
</span>
